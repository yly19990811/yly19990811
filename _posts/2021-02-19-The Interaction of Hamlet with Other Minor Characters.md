---  
layout:     post
title:     The Interaction of Hamlet with Other Minor Characters
subtitle:       
date:     2021-02-19
author:     Shuhan Wang(Eden)
header-img:     img/post-bg-rwd.jpg
catalog: true
tags:     
    - Interaction 
    - Hamlet
    - Minor Characters
---  

  The play Hamlet is a tragedy story set in the Elizabethan times, basically revolving around Hamlet, the prince of Denmark. According to the whole play, Hamlet is definitely the leading role in the whole story, as his thoughts, behaviors and actions result in the other character’s destinies and deaths, either directly or indirectly.  In this article, the interactions between Prince Hamlet and minor characters are going to further investigated. Generally speaking, the minor characters in this play are not only promote the development of the plot, but also play a significant role and as a foil in the contribution of depicting Prince Hamlet’s personality.
In Hamlet, some important story plots are narrated to Prince Hamlet and other major characters as well as to the audiences, through the actions and words of some minor characters who have a direct association with the plots. For example, the story begins with the conversations between three soldiers of Denmark: Bernardo, Francisco and Marcellus, who are the minor characters but reveal one of the key fact of the ghost. In act 1 scene 1, they come across with the ghost two times and ask the friend of Prince Hamlet, Horatio, to verify whether it is their delusion or the ghost is the soul of the late King, who is the natural father of Prince Hamlet and the older brother of King Claudius. Horatio recognize the ghost indeed is “our last king,/ whose image even but now appeared to us” and tells Prince Hamlet all about it and successfully arouse Hamlet’s disappointment to his mother and desire of revenge.
As a close friend to Prince Hamlet from the period they went to Wittenberg University together and a key characters in the play, Horatio use his devoted and sincere to showcasing that he is the most trusted friend of Hamlet. Although he is not often identified as any specific positions, he knows every detail of Hamlet’s revenge plans, and attends his friend’s plan of playing The Murder of Gonzago to Claudius is guilty of murdering his natural father. He accompanies Hamlet when they are told that Ophelia is dead. Besides those less important characters such as soldiers, he also is the only character who survives the entire play. At the end of the play, after Hamlet drinks the poisoned wine, he was bid to help tell Hamlet’s tale and put things right in Denmark: And in this harsh world draw thy breath in pain,/To tell my story (Act5, scene2).
The other important role of the minor characters in Hamlet is that they are often given the functions to make contrast to foil the flashing temperature of Prince Hamlet and other characters. One intuitive example is Rosencrantz and Guildernstern, the older friends of Hamlet during the university, they bend the knee to Claudius' corruptness and are suspected as spies. This false friendship can be considered as a direct comparison to the loyal one that is shared between Hamlet and Horatio. 
In summary, despite the fact that Horatio, Laertes, Fortinbras and other soldiers are minor characters in the play, that does not equal to that their impacts to the plots are less significant as well. In contrast, their actions and movements always have a strong and direct relevance to Prince Hamlet and the movement of the play.
![输入图片说明](https://cdn2.rsc.org.uk/sitefinity/images/productions/productions-2009-and-before/hamlet/hamlet_2008_ellie_kurttz_c_rsc_111940.tmb-gal-1340.jpg?sfvrsn=1 "在这里输入图片标题")