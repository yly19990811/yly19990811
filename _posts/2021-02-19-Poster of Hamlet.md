---  
layout:     post
title:     Poster of Hamlet
subtitle:       
date:     2021-02-19
author:     Lingyun Yang (YANLD1903)
header-img:     img/post-bg-rwd.jpg
catalog: true
tags:     
    - Poster
    - Movie of Hamlet
    -   
---  

<a data-flickr-embed="true" href="https://www.flickr.com/photos/192213770@N03/50963552618/in/dateposted-public/" title="tupian"><img src="https://live.staticflickr.com/65535/50963552618_9e57c7aa79_w.jpg" width="267" height="400" alt="tupian"></a>
This is my Hamlet poster. Firstly, I use whole dark tonal because Hamlet is a tragedy. Secondly, in the title of my poster, I decorated my title with roses because Prince Hamlet dose not have the condition of being a prince. He was imbued with humanist sentiment. For example, Hamlet missed a lot of chance in revenge, and he even consider suicide. Also, Hamlet did not kill the king because he think it is not merciful to kill him at this time. Thus, I use roses to show the humanist sentiment of Hamlet. In the middle of my poster, it has two swords and a crown. Firstly, it shows the cruelty of the crown struggle — only one person can be the king. Also, it shows the ending flight of the Hamlet. Prince Hamlet kill his enemy. At that time, he did not flincher or kindness. He was a true prince. In addition, Prince Hamlet’s head in the middle of the two swords and under the crown. It shows the contradiction in his mind. Finally, I quote the famous saying form Hamlet to make contradiction clearer. 