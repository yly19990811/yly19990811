---  
layout:     post
title:     Interpret Hamlet with Photos
subtitle:       
date:     2021-02-19
author:     Siyu Wang (Saber) SIYWD19013
header-img:     img/post-bg-rwd.jpg
catalog: true
tags:     
    - Photos 
    - Hamlet
    - Interpret
---  

I want to take a series of photos to re-construct the senses in Hamlet and show my understanding of Hamlet. I use a lot of shadows to interpret. The photos are the ghost, a play in the play, Ophelia’s funeral, the king’s death, and Hamlet’s ending. Another theme of this series of photos is shadow. There is some ambiguity in Hamlet. I think the shadow is an element that fits well with ambiguity.  
<a data-flickr-embed="true" href="https://www.flickr.com/photos/192213770@N03/50964215511/in/dateposted-public/" title="图片1"><img src="https://live.staticflickr.com/65535/50964215511_80665e89f2_b.jpg" width="662" height="1024" alt="图片1"></a>
I take this photo to symbolize the ghost. I think shadow and ghost have something in common, both of them do not exist in real life but people can see it. I edit this photo to make it greyer and make it more unreal like the ghost in Hamlet.
<a data-flickr-embed="true" href="https://www.flickr.com/photos/192213770@N03/50964216821/in/dateposted-public/" title="图片2"><img src="https://live.staticflickr.com/65535/50964216821_5a29ca61cb_c.jpg" width="575" height="800" alt="图片2"></a>
This photo is a hand holding the knife. I take this photo to symbolize the play in a play. The shadow represents the kill in play and the real hand represents Claudius kills Hamlet’s father. The shadow and the reality are different but they are naturally the same thing, just like Hamlet uses a play to verify whether his uncle killed his father.
<a data-flickr-embed="true" href="https://www.flickr.com/photos/192213770@N03/50964319267/in/dateposted-public/" title="图片3"><img src="https://live.staticflickr.com/65535/50964319267_a560d82777_c.jpg" width="800" height="607" alt="图片3"></a>
This photo represents Ophelia’s funeral. In act V scene one Shakespeare suddenly changes the view from Hamlet and focuses on the gravedigger, use an objective attitude to express Ophelia’s death. I take this photo from the top to imitate an objective view.
<a data-flickr-embed="true" href="https://www.flickr.com/photos/192213770@N03/50964217101/in/dateposted-public/" title="图片4"><img src="https://live.staticflickr.com/65535/50964217101_617a06f8dc_w.jpg" width="399" height="277" alt="图片4"></a>
This photo represents the death of king Claudius. The chair falls represent his right is ending. I edit the color of the picture to make the red in shadow more distinct to show that the wine is poisoned.
<a data-flickr-embed="true" href="https://www.flickr.com/photos/192213770@N03/50964319457/in/dateposted-public/" title="图片5"><img src="https://live.staticflickr.com/65535/50964319457_825375c1a7_w.jpg" width="342" height="400" alt="图片5"></a>
This picture represents the death of Hamlet. I use dark red color to Hamlet is dying. The sword downward uses the sword to support his body. The two shadows of the sword represent the costs like fights and death of choosing revenge.