---  
layout:     post
title:     Hamlet Research
subtitle:     
date:     2021-02-19
author:     Zhiyuan Yin (YINZD1903)
header-img:     img/post-bg-rwd.jpg
catalog: true
tags:     
    - Research
    - Hamlet
    - Shakespeare
---  

Speaking of Hamlet, the most classic sentence "a thousand readers, a thousand
Hamlets" is the most accurate. In the whole play, revenge is intertwined with human
nature, irresistible fate and brave personal pursuit impact the hearts of the
characters, poetic language contains the romance of the middle ages and the
Renaissance.  
Hamlet has always existed in the history of literature as a complex tragic figure.
Perhaps the reason why Hamlet is so intriguing is the complexity and multiplicity of
his character. Hamlet, a humanist thinker in the contradiction between ideal and
reality, was stimulated by great changes overnight. His inner shame and hesitation
made the prince embark on the journey of melancholy.  
Before all the bad luck, Hamlet lived in the court of an enlightened monarch, an ideal
kingdom full of sunshine and fantasy, and accepted western humanism. In his eyes,
everything he saw was so beautiful. However, after the sudden death of his father,
the capture of his uncle's throne, the remarriage of his mother, and the narration of
his ghost, the happy prince became melancholy. In extreme sorrow, he regarded the
world as a "barren garden, full of poisonous weeds". The reality that he could see
was full of evil and ugliness, and the beautiful world of the past disappeared in his
mind. This kind of hesitation also runs through the whole drama. Ophelia, a kind and
beautiful noble girl, had fallen in love with Hamlet long before everything changed.
She was happy. In the end, she was waiting for a prince who could only play a fool
and seemed to lose his mind. Of course, Hamlet's heart is also extremely painful, so
that later through the vigorous love letter to suggest that she, he hopes his love can
understand, understand a revenge for his father's heart. Ophelia can only feel deep
love, she is just intoxicated with her fairy tales, her dreams. However, the reality as
heavy as iron finally shattered all the illusions. When Hamlet killed Ophelia's father by
mistake, she was crazy. She couldn't accept the fact that her beloved prince killed
her father. For her, Hamlet and her father were her true love. She ran to the place
where she dated her beloved prince madly At this time, Hamlet does not know the
current situation of Ophelia. At this time, he just escaped from Claudius. When he
returned to his castle again, he saw Ophelia's funeral wrapped in flowers. This is
Hamlet's love tragedy.
In the play, Hamlet's friendship is tragic. When Leotis, Ophelia's brother, saw Hamlet
at his sister's funeral, he was angry, only angry, which made him used by the evil
Claudius and created a tragedy for later generations. When Leotis stabbed Hamlet
with his poisoned sword, when Jotrude died of poisoning, and when Leotis was
stabbed with his own poisoned sword, he told Hamlet about the plot of sword
competition and then died with repentance. For Leotis, when he knew what happened
to Hamlet, when he knew that his sister loved Hamlet deeply, all this should not have
happened to him as a friend, but the impulse, anger and broken family broke the
friendship between him and Hamlet. If the fate is not so arranged, if it is not a
corrupt and disorderly society, where there will be friends of fratricidal!
![输入图片说明](https://www.aicinema.com.br/wp-content/uploads/2014/07/Hamlet-12.jpg "在这里输入图片标题")