---  
layout:     post
title:     Vengeance, Mad or Heroic?
subtitle:       
date:     2021-02-19
author:     Yiqun Xue (XUEY4D1703)
header-img:     img/post-bg-rwd.jpg
catalog: true
tags:     
    - Personal Opinion
    - Hamlet
    - Comparison 
---  

“To be or not to be, that’s the question.” This famous quote from hamlet’s analog accurately described his inner thoughts. After all this time drowning in grief due to his father’s mystery death, how come a man would remain calm when the missed one revived as a ghost and requested his own son to revenge for him.

Hamlet was just a young man, as young as us. Beautiful things that would normally occur to young people occur to this Denmark prince as well. He fell in love, he had strong friendship and a loving mother. However, the young lover became victim just because she is the daughter of Hamlet’s rival force. His friend is loyal till the end and willing to die with him along. His mother was blind and powerless, left him less room to decide how to revenge. 

Is Hamlet a hero? Yes, he was willing to sacrifice things that he cherishes and chose to find his father justice. He had brilliant fighting skills, advanced knowledge and extraordinary bravery. He’d die for his father, his country and the justice he trusts.

Is Hamlet a maniac? Yes, he would rather trust a ghost that claimed to be his father’s will than listening to his own living mother’s wishes. He abandoned his love for vengeance, felt no regret after accidentally kill his lover’s father, and chose to perform the most violent way to achieve his revenge. Could it all be his own hallucination? Since he was dying to do something after the death of the king just because he could not learn to accept it and move on.

He said to himself, to be or not to be, that’s the question. Is this justice he was dying to see worth all the sacrifices? Is he a great prince and able to do the best he could? Or is he just a normal young person driven by hopeless and void. There’s little distance between hero and insanity. He decided to be him. 
![输入图片说明](https://tse2-mm.cn.bing.net/th/id/OIP.4x4UqY-pnHXm4IflINAneAHaE8?pid=ImgDet&rs=1 "在这里输入图片标题")